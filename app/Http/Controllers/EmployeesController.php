<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class EmployeesController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

//		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resourse.
	 *
	 * @return Response
	 */
	public function indexGrid(){
		$key = Input::get('key','');
		$emp_obj = new Employees();
		$employees = $emp_obj->paginateEmployees(9, empty($key)? 'full_name': $key);
		return view('employees.index_grid',array('employees' => $employees));
	}

	public function index(){
		$emp_obj = new Employees();
		$employees = $emp_obj->paginateEmployees(10);

		return view('employees.index',array('employees' => $employees));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		$employees = new Employees();
		$chiefs = $employees->getChiefs();
		return view('employees.create', compact('chiefs', $chiefs));
	}

	/**
	 * Display the specified resource
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id){
		return view('');
	}

	/**
	 *Store a newly created resource in storage
	 *
	 * @return Response
	 */

	public function store(Request $request){
		$employee = new Employees();
		$employee->full_name = $request->full_name;
		$employee->position = $request->position;
		$employee->date_start_work = $request->date_start_work;
		$employee->salary = $request->salary;
		$employee->chief_id = $request->chief_id;
		$employee->photo_id = $request->photo_id;

		$employee->save();

		return redirect()->action('EmployeesController@index');
	}

	/**
	 * Show the from for editing the specified resource
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id){
		$employees = new Employees();
		$employee = $employees::with('photo')->find($id);
		$chiefs = $employees->getChiefs();
		return view('employees.edit', compact('employee', $employee, 'chiefs', $chiefs));
	}

	/**
	 * Update the specified resource from storage
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$employee = Employees::find($id);

		$employee->full_name = $request->full_name;
		$employee->position = $request->position;
		$employee->date_start_work = $request->date_start_work;
		$employee->salary = $request->chief_id;

		$employee->save();

		return redirect()->route('employees.index');
	}

	/**
	 * Remove the specified resource from storage
	 *
	 * @param int $id
	 * @return Response
	 */
	public function destroy($id){
		if(!empty($id)) {
			$emp_obj = new Employees();
			$emp_obj->deleteById($id);

//			Employees::find($id)->delete();
		}
		return redirect()->route('employees.index');
	}

	public function tree(){

		$employees =  Employees::with('employees','employees.employees')->find(1);

		$tree_array[0]['text'] = $employees->full_name . ' #' . $employees->position;
		$tree_array[0]['nodes'] = $this->makeTreeArray($employees, 'employees');

		return view('employees.index_tree', ['employees' => json_encode($tree_array)]);
	}

	private function makeTree($cats, $parent, $only_parent = false){
		if(!empty($cats) and isset($cats->$parent)){
			$tree = '<ul>';
			if($only_parent == false){
				foreach($cats->$parent as $cat){
					$tree .= '<li>'.$cat->full_name.' #'.$cat->position;
					$tree .= $this->makeTree($cat, $parent);
					$tree .= '</li>';
				}
			} elseif(is_numeric($only_parent)){
				$cat = $cats[$parent][$only_parent];
				$tree .= '<li>'.$cat->full_name.' #'.$cat->position;
				$tree .=  $this->makeTree($cats, $parent);
			}
			$tree .= '</ul>';
		} else return null;

		return $tree;
	}

	private function makeTreeArray($cats, $parent, $only_parent = false){
		if(!empty($cats) and isset($cats->$parent)){
			$tree = [];
			if($only_parent == false){
				foreach($cats->$parent as $key => $cat){
					$tree[$key]['text'] = $cat->full_name.' #'.$cat->position;
					$branch = $this->makeTreeArray($cat, $parent);
					if(!empty($branch)){
						$tree[$key]['nodes'] = 	$branch;
					}
				}
			}
		} else return null;

		return $tree;
	}

	public function search(){
		$keyword = Input::get('keyword', '');
		$page = Input::get('page', '');
		$employee_obj = new Employees();
		$employees = $employee_obj->searchEmployees('full_name', $keyword, 10);
		if(empty($page)) {
			return view('employees.index_table', array('employees' => $employees, 'keyword' => $keyword));
		} else {
			return view('employees.index', array('employees' => $employees, 'keyword' => $keyword));
		}
	}

}

