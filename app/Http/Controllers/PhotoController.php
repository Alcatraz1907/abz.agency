<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

use App\Http\Requests;
use Symfony\Component\Console\Input\Input;

class PhotoController extends Controller
{
	public function upload_photo(Request $request){
		$photo = $this->makePhoto($request->file('photo'));
		$photo_obj = new Photo();
		$result_photo = $photo_obj->addPhoto($photo);

		return response()->json([
			'photo_id'       => $result_photo->id,
			'path'           => $result_photo->path,
			'thumbnail_path' => $result_photo->thumbnail_path,
		]);
	}

	public function makePhoto(UploadedFile $file){
		return Photo::named($file->getClientOriginalName())->move($file);
	}

}
