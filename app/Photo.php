<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class Photo extends Model
{
	protected $fiellable = ['name', 'path', 'thumbnail_path'];
	protected $base_dir = 'img/photos';

	public function employees(){
		return $this->belongsTo('App\Employees');
	}

	public static function named($name){
		return (new static)->saveAs($name);
	}

	public function saveAs($name){
		$this->name = $name;
		$this->path = sprintf('%s/%s', $this->base_dir, $this->name);
		$this->thumbnail_path = sprintf('%s/th-%s', $this->base_dir, $this->name);

		return $this;
	}

	public function move(UploadedFile $file){
		$file->move($this->base_dir, $this->name);

		$this->makeThumbnail();
		$this->resizePhoto();

		return $this;
	}

	public function resizePhoto(){
		Image::make($this->path)
			->fit(900)
			->save($this->path);
	}

	public function makeThumbnail(){
//		Image::make(public_path('img\photos\\').$this->name)
		Image::make($this->path)
			->fit(250)
//			->save(public_path('img\photos\th-').$this->name);
			->save($this->thumbnail_path);
	}

	public function addPhoto(Photo $photo){
		$photo->save();
		return $photo;
	}
}
