<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class Employees extends Model
{

	public function employees(){
		return $this->hasMany('App\Employees', 'chief_id', 'id');
	}

	public function photo(){
		return $this->hasOne('App\Photo', "id", 'photo_id');
	}

//	public function __construct(){
//		parent::__construct();
//		$this->table = 'employees';
//	}
    public function getChiefs(){
		return $this->
			select(DB::raw('id, full_name, position, chief_id'))
			->whereIn('id',  function($query) {
				$query->select(DB::raw('chief_id'))
					->from('employees');
			})
			->get();
	}

	public function paginateEmployees($count, $key = 'full_name'){
		if(!empty($count)) {
			return $this->
			with('photo')->
			select(DB::raw('*'))
				->whereNotIn('id', function ($query) {
					$query->select(DB::raw('chief_id'))
						->from('employees');
				})
				->orderBy($key, 'asc')
				->paginate($count);
		}
		return [];
	}

	public function getEmployees(){
		return $this->select(DB::raw('id, full_name, position, chief_id'))
			->orderBy('chief_id', 'ASC')
			->get();
	}

	public function searchEmployees($key, $word, $count = false){
		if(!empty($count)) {
			return $this->select(DB::raw('*'))
				->whereNotIn('id', function ($query) {
					$query->select(DB::raw('chief_id'))
						->from('employees');
				})
				->where($key, 'LIKE', "%$word%")
				->orWhere("position", "LIKE", "%$word%")
				->orWhere("date_start_work", "LIKE", "%$word%")
				->orWhere("salary", "LIKE", "%$word%")
				->paginate($count);
		}
		return [];
	}

	public function scopeSearchByKeyword($query, $keyword)
	{
		if ($keyword!='') {
			$query->where(function ($query) use ($keyword) {
				$query->where("full_name", "LIKE","%$keyword%")
					->orWhere("position", "LIKE", "%$keyword%")
					->orWhere("date_start_work", "LIKE", "%$keyword%")
					->orWhere("salary", "LIKE", "%$keyword%");
			});
		}
		return $query;
	}

	public function deleteById($id){
		$data = $this->
				select(DB::raw('id, full_name, position, chief_id'))
					->whereIn('id',  function($query) {
						$query->select(DB::raw('chief_id'))
							->from('employees');
					})
				->where('id', $id)
				->get();
		if(isset($data[0]) && $data[0]->chief_id != 0){
			$this->where('chief_id', '=', $id)
				->update(['chief_id' => $data[0]->chief_id]);

			$this->find($id)->delete();
		} else {
			$this->find($id)->delete();
		}
	}
}
