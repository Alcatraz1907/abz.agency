@extends('layouts/app')

@section('content')

	<div class="row">
		<h1>Edit Employee</h1>
		<div class="col-md-12">
		 <form role="form" method="POST" action="{{url('employees')}}">
		 {{csrf_field()}}
			 <input type="hidden" name="photo_id" value="{{$employee->photo->id}}">
			<div class="form-group">
			  <label for="full_name">Full name:</label>
			  <input type="text" name="full_name" class="form-control" value="{{$employee->full_name}}">
			</div>
			<div class="form-group">
			  <label for="position">Position:</label>
			  <input type="text" name="position" class="form-control" value="{{$employee->position}}">
			</div>
			<div class="form-group">
			  <label for="date_start_work">Date start work:</label>
			  <input type="date" name="date_start_work" class="form-control" value="{{$employee->date_start_work}}">
			</div>
			<div class="form-group">
				<label for="salary">salary:</label>
				<input type="number" name="salary" class="form-control" value="{{$employee->salary}}">
		  </div>
		  <div class="form-group">
				<label for="photo">Photo:</label>
				<input type="file" name="photo" class="mainImage form-control" required>
		  </div>
		  <div class="imgBlock">
		  	<img src="{{url($employee->photo->path)}}">
		  </div>
		  <div class="form-group">
				<label for="chief_id">Select chief:</label>
				 <select class="form-control" id="chief_ids" name="chief_id">
				 						{{$employee->id}}

					@foreach($chiefs as $chief)
						@if($chief->id == $employee->chief_id)
							<option selected value="{{$chief->id}}">{{$chief->full_name}}</option>
					 	@else
							<option value="{{$chief->id}}">{{$chief->full_name}}</option>
					 	@endif
					 @endforeach
				   </select>
		  </div>
			<button type="submit" class="btn btn-default">Submit</button>
		  </form>
		</div>
	</div>
@stop