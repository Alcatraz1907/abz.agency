@extends('layouts/app')

@section('content')
	{{--<div class="row">--}}
	  {{--<div class="col-sm-1">id</div>--}}
	  {{--<div class="col-sm-1"></div>--}}
	  {{--<div class="col-sm-1">Full name</div>--}}
	  {{--<div class="col-sm-1">Position</div>--}}
	  {{--<div class="col-sm-1">Date start work</div>--}}
	  {{--<div class="col-sm-1">salary</div>--}}
	  {{--<div class="col-md-1">chief_id</div>--}}
	{{--</div>--}}
	<div class="well well-sm">
		<strong>Display grid employees</strong>

		<form method="post" class="form-inline" action="{{url('employees/grid')}}">
			 {{csrf_field()}}
			<div class="form-group">
              <label >Select list:</label>
              <select class="form-control" name="key" >
                <option value="full_name">Full name</option>
                <option value="position">Position</option>
                <option value="salary">Salary</option>
                <option value="date_start_work">Date</option>
              </select>
            </div>
            <button class="btn btn-primary">Submit</button>
		</form>
	</div>
	<div id="employees" class="row list-group">
		@foreach(array_chunk($employees->getCollection()->all(), 3) as $row)
			<div class="row">
			@foreach($row as $item)
				<div class="item  col-xs-4 col-lg-4">
					<div class="thumbnail">
						<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
						<div class="caption">
							<h4 class="group inner list-group-item-heading">
								{{$item->full_name}}</h4>
							<p class="group inner list-group-item-text">
								Position: {{$item->position}}</p>
							<div class="row">
								<div class="col-xs-12 col-md-6">
									<p class="lead">
										${{$item->salary}}</p>
								</div>
								<div class="col-xs-12 col-md-6">
									<a class="btn btn-success" href="{{url('employees/'.$item->id.'/edit')}}">Edit</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		@endforeach
	</div>
		{{$employees->render()}}
@stop