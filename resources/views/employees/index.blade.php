@extends('layouts/app')

@section('content')
		<form class="form-inline searchForm" >
			 {{csrf_field()}}
			<div class="form-group">
				<input type="text" name="keyword" value="" class="form-control" required>
			</div>
			<button type="submit" class=""><span class="glyphicon glyphicon-search"></span></button>
		</form>
		<div class="w3-padding w3-whsite notranslate employeesTable">

        <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>photo</th>
              <th>Full name</th>
              <th>Position</th>
              <th>Date start work</th>
              <th>Salary</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
         	 @foreach($employees as $item)
				<tr class="success">
				  <td>{{$item->id}}</td>
				  <td><img src="{{url($item->photo->thumbnail_path)}}"></td>
				  <td>{{$item->full_name}}</td>
				  <td>{{$item->position}}</td>
				  <td>{{$item->date_start_work}}</td>
				  <td>{{$item->salary}}</td>
				  <td>
					 <form action="{{url('employees/'.$item->id)}}" method="POST">
					 	{{ csrf_field() }}
						{{ method_field('DELETE') }}

						<a href="{{url('employees/'.$item->id.'/edit')}}" class="btn btn-warning">Edit</a>
						{{--<input type="hidden" name="id" value="{{$item->id}}">--}}
						<button type="submit" class="btn btn-danger">Del</button>
					 </form>
			   	  </td>
				</tr>
            @endforeach
          </tbody>
        </table>
			{{$employees->render()}}
    </div>
@stop