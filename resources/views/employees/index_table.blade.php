<table class="table">
	<thead>
	<tr>
		<th>Id</th>
		<th>Full name</th>
		<th>Position</th>
		<th>Date start work</th>
		<th>Salary</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
	@foreach($employees as $item)
	<tr class="success">
		<td>{{$item->id}}</td>
		<td>{{$item->full_name}}</td>
		<td>{{$item->position}}</td>
		<td>{{$item->date_start_work}}</td>
		<td>{{$item->salary}}</td>
		<td>
			<form action="{{url('employees/'.$item->id)}}" method="POST">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<a href="{{url('employees/'.$item->id.'/edit')}}" class="btn btn-warning">Edit</a>
				{{--<input type="hidden" name="id" value="{{$item->id}}">--}}
				<button type="submit" class="btn btn-danger">Del</button>
			</form>
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
{{$employees->render()}}