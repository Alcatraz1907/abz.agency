@extends('layouts/app')

@section('content')

	<div class="row">
		<h1>Create Employee</h1>
		<div class="col-md-12">
		 <form role="form" method="POST" action="{{url('employees')}}" class="createEmployees">
		 {{csrf_field()}}
		 <input type="hidden" name="photo_id" value="">
           <div class="form-group">
             <label for="full_name">Full name:</label>
             <input type="text" name="full_name" class="form-control">
           </div>
           <div class="form-group">
             <label for="position">Position:</label>
             <input type="text" name="position" class="form-control">
           </div>
           <div class="form-group">
             <label for="date_start_work">Date start work:</label>
             <input type="date" name="date_start_work" class="form-control">
           </div>
           <div class="form-group">
				<label for="salary">salary:</label>
				<input type="number" name="salary" class="form-control">
		  </div>
		  <div class="form-group">
				<label for="photo">Photo:</label>
				<input type="file" name="photo" class="mainImage form-control" required>
		  </div>
		  <div class="imgBlock">
		  </div>
		  <div class="form-group">
				<label for="chief_id">Select chief:</label>
				 <select class="form-control" id="chief_ids" name="chief_id">
				 	@foreach($chiefs as $chief)
						<option value="{{$chief->id}}">{{$chief->full_name}}</option>
                    @endforeach
                  </select>
		  </div>
           <button type="submit" class="btn btn-default">Submit</button>
         </form>
		</div>

  	</div>
@stop