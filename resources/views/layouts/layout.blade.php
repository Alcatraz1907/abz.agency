 <!doctype html>
 <html lang="en-US">
 <head>
   <meta charset="UTF-8">
   <title>Photo gallery</title>
   <link rel="stylesheet" href="/css/bootstrap.css">
   <link rel="stylesheet" href="/css/bootstrap-theme.css">
   {{--<link rel="stylesheet" href="/css/app.css">--}}
 </head>
 <body>
	@include("layouts.side_bar")
	<div class="container">
		@yield("content")
	</div>
 	<script src="/js/jquery-1.12.3.min.js"></script>
 	<script src="/js/bootstrap.js"></script>
 	<script src="/js/bootstrap-treeview.js"></script>
 	<script src="/js/tree_data.js"></script>
 	<script src="/js/scripts.js"></script>
 	{{--<script src="/js/app.js"></script>--}}
 </body>
 </html>