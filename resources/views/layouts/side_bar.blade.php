<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-default">
  <div class="container">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="#">Bootstrap theme</a>
	</div>
	<div id="navbar" class="navbar-collapse collapse">
	  <ul class="nav navbar-nav">
		<li class="dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employees <span class="caret"></span></a>
		  <ul class="dropdown-menu">
			<li><a href="{{url('employees/grid')}}">Grid list</a></li>
			<li><a href="{{url('employees/create')}}">Create</a></li>
			<li><a href="{{url('employees')}}">Table list</a></li>
			<li><a href="{{url('employees/tree')}}">Tree list</a></li>
		  </ul>
		</li>
	  </ul>
	</div><!--/.nav-collapse -->
  </div>
</nav>