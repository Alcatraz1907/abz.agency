<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Employees::class, function (Faker\Generator $faker) {
	return [
		'full_name' => $faker->name,
		'position' => $faker->sentence(),
		'date_start_work' => $faker->date(),
		'salary' => $faker->randomFloat(8, 300, 5000),
		'chief_id' => rand(1,5),
		'photo_id' => rand(1,1000),

	];
});

$factory->define(App\Photo::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->imageUrl(250, 250),
		'path' => $faker->imageUrl(500, 500),
		'thumbnail_path' => $faker->imageUrl(200, 200),
	];
});
