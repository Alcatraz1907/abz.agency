<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('photos', function (Blueprint $table) {
			$table->increments('id');
//			$table->foreign('id')->references('photo_id')->on('employees')->onDelete('Cascade');

			$table->string('name', 120);
			$table->string('path', 120);
			$table->string('thumbnail_path', 120);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}
}
