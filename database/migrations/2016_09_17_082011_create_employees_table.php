<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function (Blueprint $table) {
			$table->increments('id');
			$table->string('full_name');
			$table->string('position');
			$table->date('date_start_work');
			$table->float('salary');
			$table->integer('chief_id');
			$table->integer('photo_id')->unsigned();
			$table->foreign('photo_id')->references('id')->on('photos');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}
}
