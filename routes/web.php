<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('employees/tree', ['uses' => 'EmployeesController@tree']);

	Route::group(['middleware' => 'auth'], function () {
		Route::get('employees/grid', ['uses' => 'EmployeesController@indexGrid']);
		Route::post('employees/grid', ['uses' => 'EmployeesController@indexGrid']);
		//Route::get('employees/table', ['uses' => 'EmployeesController@index']);
		Route::get('employees/create', ['uses' => 'EmployeesController@create']);
		Route::post('employees/search', ['uses' => 'EmployeesController@search']);
		Route::get('employees/search', ['uses' => 'EmployeesController@search']);
//		Route::post('photo/upload_photo', 'PhotoController@upload_photo');
		Route::post('photo/upload_photo', ['uses' => 'PhotoController@upload_photo']);

		Route::resource('employees', 'EmployeesController');

	});

Auth::routes();
Route::get('/home', 'HomeController@index');

